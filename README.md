# Anti-c++ patterns

This repo contains c++ patterns that should be avoided.  For more details,
please refer to my [blog post](https://twdev.blog/2022/06/cpp_antipatterns/).

