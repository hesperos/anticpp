#include <benchmark/benchmark.h>

int foo_with_static_int() {
    static const int i = 123;
    return i;
}

int foo_without_static_int() {
    const int i = 123;
    return i;
}

int vector_with_static() {
    static const std::vector<int> v = {
        1,2,3,4,5,6,7,8,9,10
    };

    return v[4];
}

int vector_without_static() {
    const std::vector<int> v{
        1,2,3,4,5,6,7,8,9,10
    };

    return v[4];
}

static void BM_foo_with_static_int(benchmark::State& state) {
    for (auto _ : state) {
        foo_with_static_int();
    }
}

static void BM_foo_without_static_int(benchmark::State& state) {
    for (auto _ : state) {
        foo_without_static_int();
    }
}

static void BM_vector_with_static(benchmark::State& state) {
    for (auto _ : state) {
        vector_with_static();
    }
}

static void BM_vector_without_static(benchmark::State& state) {
    for (auto _ : state) {
        vector_without_static();
    }
}


BENCHMARK(BM_foo_with_static_int);
BENCHMARK(BM_foo_without_static_int);
BENCHMARK(BM_vector_without_static);
BENCHMARK(BM_vector_with_static);

BENCHMARK_MAIN();
